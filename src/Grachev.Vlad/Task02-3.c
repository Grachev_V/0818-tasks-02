#include <stdio.h>
#include <time.h>
#define N 10

int SrchPer(int (*arr)[N])
{
	int i,j,k, maxper = 1, locper = 1;
	for(k = 0; k < N; k++)
		for(i = 0; i < N-1; i++){
			for(j = i + 1; j < N; j++)
				if(arr[k][i] == arr[k][j])
					locper++;
				if (maxper < locper)
					maxper = locper;
			locper = 1;
		}
	return maxper;	
}

int main()
{
	int array[N][N];
	int i,j;
	srand(time(0));
	for(i = 0; i < N; i++){
		for(j = 0; j < N; j++){
			array[i][j] = rand() % (N / 2) + 1;
			printf("%3d ", array[i][j]);
		}
		printf("\n");
	}
	printf("Result: %d\n", SrchPer(array));
	return 0;
}